from django.db import models

from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    created_at = models.DateTimeField(
        verbose_name=_('Created at'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=_('Updated at'),
        auto_now=True
    )
    is_active = models.BooleanField(
        verbose_name=_('Is Active'),
        default=True
    )

    class Meta:
        abstract = True


class PositionModel(models.Model):
    position = models.SmallIntegerField(
        verbose_name=_('Position'),
        default=0
    )

    class Meta:
        abstract = True
