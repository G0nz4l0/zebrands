from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from apps.common.models import BaseModel


class QueryEvent(BaseModel):
    user_id = models.CharField(max_length=10)
    product_id = models.CharField(max_length=5)
    brand_id = models.CharField(max_length=3)

    class Meta:
        verbose_name = _('Query Event')
        verbose_name_plural = _('Query Events')
        ordering = ('-created_at',)
