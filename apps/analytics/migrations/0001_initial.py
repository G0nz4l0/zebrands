# Generated by Django 3.2.12 on 2022-03-12 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='QueryEvent',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated at')),
                ('is_active', models.BooleanField(default=True, verbose_name='Is Active')),
                ('user_id', models.CharField(max_length=10)),
                ('product_id', models.CharField(max_length=5)),
                ('brand_id', models.CharField(max_length=3)),
            ],
            options={
                'verbose_name': 'Query Event',
                'verbose_name_plural': 'Query Events',
                'ordering': ('-created_at',),
            },
        ),
    ]
