import requests

import config.settings.base
from apps.analytics.models import QueryEvent
from apps.users.models import User
from apps.users import constants as user_constants

from django.core.mail import send_mail


# this could be a background task
def create_product_query_event(user, product):
    QueryEvent.objects.create(
        user_id=user.id,
        product_id=product.id,
        brand_id=product.brand_id
    )


def send_email_notification(payload: dict):
    send_mail(
                payload.get('subject'),
                payload.get('message'),
                payload.get('email_from'),
                payload.get('list_email_to'),
                fail_silently=False,
            )


def send_slack_notification(message):
    url = config.settings.base.SLACK_URL_NOTIFICATION_EVENTS
    requests.post(url, json={'text': message})


def get_admin_emails():
    admin_email_list = list(User.objects.filter(type='TYPE_ADMIN').values_list('email', flat=True).distinct())
    return list(filter(bool, admin_email_list))
