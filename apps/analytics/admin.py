from django.contrib import admin

from apps.analytics.models import QueryEvent


# Register your models here.
@admin.register(QueryEvent)
class QueryEventAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'product_id', 'brand_id')
    list_filter = ('user_id', 'product_id', 'brand_id')
    search_fields = ('user_id', 'product_id', 'brand_id')
