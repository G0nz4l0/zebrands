__version__ = "0.1.0"
__version_info__ = tuple(
    [
        int(num) if num.isdigit() else num
        for num in __version__.replace("-", ".", 1).split(".")
    ]
)

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UsersConfig(AppConfig):
    name = "apps.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import apps.users.signals  # noqa F401
        except ImportError:
            pass


class ProductsConfig(AppConfig):
    name = 'apps.products'
    verbose_name = _('Products')


class AnalyticsConfig(AppConfig):
    name = 'apps.analytics'
    verbose_name = _('Analytics')


class NotificationsConfig(AppConfig):
    name = 'apps.notifications'
    verbose_name = _('Notifications')
