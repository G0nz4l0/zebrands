from django.contrib import admin

from apps.products.models import Product, Brand


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at', 'is_active')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'sku', 'name')
    list_filter = ('brand',)
    search_fields = ('name', 'sku')
