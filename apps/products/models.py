from django.db import models
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from apps.common.models import BaseModel


class Brand(BaseModel):
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=100
    )

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = _('Brand')
        verbose_name_plural = _('Brands')


class Product(BaseModel):
    sku = models.CharField(
        verbose_name=_('sku'),
        max_length=20,
        db_index=True,
        null=True,
        blank=True
    )
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=100
    )
    price = models.FloatField(
        verbose_name=_('Price')
    )
    brand = models.ForeignKey(
        'products.Brand',
        verbose_name=_('Brand'),
        related_name='products',
        on_delete=models.PROTECT
    )

    def __str__(self):
        return f'{self.name } - {self.sku}'

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        ordering = ('-created_at',)

