from django.conf.urls import url

from apps.products.api.views import ListCreateProductAPIView, RetrieveUpdateDestroyProductAPIView

urlpatterns = [
    url(r'^api/products/$', ListCreateProductAPIView.as_view(), name='list-create-products'),
    url(r'^api/products/(?P<product_id>\d+)/$', RetrieveUpdateDestroyProductAPIView.as_view(),
        name='retrieve-update-destroy-products')
]
