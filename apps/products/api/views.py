# Create your views here.
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

import config.settings.base
from apps.analytics.utils import create_product_query_event, send_email_notification, get_admin_emails, send_slack_notification
from apps.products.api.serializers import ProductSerializer
from apps.products.models import Product
from apps.users.permissions import IsAdmin, IsAdminOrAnonymous


class ListCreateProductAPIView(ListCreateAPIView):
    '''list and create products api'''

    permission_classes = [IsAuthenticated, IsAdmin]
    serializer_class = ProductSerializer

    def get_queryset(self):
        return Product.objects.filter(is_active=True).select_related('brand')


class RetrieveUpdateDestroyProductAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProductSerializer

    def dispatch(self, request, *args, **kwargs):
        if request.method == "GET":
            self.permission_classes = (IsAuthenticated, IsAdminOrAnonymous)
        else:
            self.permission_classes = (IsAuthenticated, IsAdmin)
        return super(RetrieveUpdateDestroyProductAPIView, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        product_id = self.kwargs.get('product_id')
        return get_object_or_404(
            Product.objects.filter(is_active=True).select_related('brand'),
            id=product_id
        )

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        create_product_query_event(self.request.user, instance)
        return Response(serializer.data)

    def perform_update(self, serializer):
        changed_values = self.request.data
        product = serializer.save()
        message = 'Se modificaron los siguientes valores: {}'.format(str(changed_values))
        subject = 'Producto <{}> modificado'.format(product)
        payload = {
            'subject': subject,
            'message': message,
            'email_from': config.settings.base.BUSINESS_EMAIL,
            'list_email_to': get_admin_emails()
        }
        send_email_notification(payload)
        send_slack_notification('{} : {}'.format(subject, message))

