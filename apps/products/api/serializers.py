from django.forms import model_to_dict
from rest_framework.serializers import ModelSerializer, RelatedField

from apps.products.models import Brand, Product


class BrandSerializer(ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id', 'name')


class BrandField(RelatedField):
    def to_representation(self, obj):
        serializer = BrandSerializer(obj)
        return serializer.data

    def to_internal_value(self, pk):
        return Brand.objects.get(id=pk)


class ProductSerializer(ModelSerializer):
    # brand = BrandSerializer()
    brand = BrandField(queryset=Brand.objects.filter(is_active=True))

    class Meta:
        model = Product
        fields = ('id', 'sku', 'name', 'price', 'brand')
