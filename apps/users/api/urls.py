from django.conf.urls import url

from apps.users.api.views import LoginAPIView

urlpatterns = [
    url(r'api/login/$', LoginAPIView.as_view(), name='login')
]
