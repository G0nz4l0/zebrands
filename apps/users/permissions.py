from rest_framework import permissions

from apps.users.constants import TYPE_ADMIN, TYPE_ANONYMOUS


class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.type == TYPE_ADMIN


class IsAdminOrAnonymous(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.type in (TYPE_ADMIN, TYPE_ANONYMOUS)
