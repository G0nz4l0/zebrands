# Generated by Django 3.2.12 on 2022-03-12 08:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='type',
            field=models.CharField(choices=[('TYPE_ADMIN', 'TYPE ADMIN'), ('TYPE_ANONYMOUS', 'TYPE ANONYMOUS')], default='TYPE_ADMIN', max_length=50),
        ),
    ]
