from apps.users import strings

TYPE_ADMIN = 'TYPE_ADMIN'
TYPE_ANONYMOUS = 'TYPE_ANONYMOUS'

USER_TYPES = (
    (TYPE_ADMIN, strings.TYPE_ADMIN),
    (TYPE_ANONYMOUS, strings.TYPE_ANONYMOUS),
)
