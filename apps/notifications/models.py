from django.db import models

# Create your models here.
from apps.common.models import BaseModel


class Notification(BaseModel):
    sender = models.ForeignKey(
        'users.User',
        related_name='notifications',
        on_delete=models.SET_NULL
    )
    recipients = models.ManyToManyField(
        'users.User',
        # through='NotificationUser',
        related_name='received_notifications'
    )
    type = models
